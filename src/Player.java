public class Player {
    String name ;
    int score;
    private static Player p =null;
    private Player(){
    }
    public static Player getInstance(){
        if (p==null) {
            p = new Player();
        }
        return p ;
    }
}
